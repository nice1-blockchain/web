# NICE1 BLOCKCHAIN

### Shaking up video game ecosystem




## ABSTRACT
 
Current video game industry is totally centralized: video games direction is lead by few but very big companies while there are small companies that have scarce options to grow and compete in equal conditions with those. Meanwhile, video game users have almost no relevance in the video game ideation and development phases and they do not have a real ownership of their acquired products. Financing, ownership and management rights are some of the factors that maintain the current establishment. Moreover, among the vast amount of video game related blockchains, there are no projects with community-driven ecosystem philosophy. Nice1 blockchain is focused on the development of an ecosystem where blockchain is implemented in video games in order to revert current establishment. Nice1 aims to encourage the implementation of blockchain usage within video games field, giving players a real ownership of their acquired products as well as the opportunity to show an active role in video games ideation and development process, giving developers funding for the development of community liked projects without giving up their ownership and management rights and, moreover, promoting the development of tools that facilitates blockchain usage and its video games implementation for any video game developer despite its blockchain knowledge. This ecosystem will be initially managed by a Foundation which role will be taken down by the community along the next years. Foundation management will be fully transparent and the accounting ledger will be accessible for anybody. With its sustainable and healthy tokenomics, its democratic philosophy and its ecosystemic vision, Nice1 is not aimed to be another video games blockchain, its aimed to be the first and definitive blockchain and video games community driven ecosystem.





## PROBLEM 

### BACKGROUND

Nowadays, there are big corporations and video-game studies choosing which projects are suitable to be funded and promoted and which are not. Those are business, are designed to earn as much money as possible and, therefore, their main reason to promote or not a videogame project is profitability. The sad part is that there might be high quality but less profitable video-game projects which are doomed to do not get funded nor promoted, meaning that will never be developed. Likewise, developers need good connections with those corporations and video-game studies if they want to get success. In this line, the firsts might have to show or even to feign values and philosophy in line with the study and, going deeper, might also accept unnecessary changes in their projects for the main reason to make them more profitable to the detriment of quality or the original idea. Nevertheless, there remain developers that will never give up their principles and values, that have clearly ideals about how video-games should be developed despite big studies claims. Taking into account this “normality”, gamers have only a role in the ecosystem: to take it or to leave it. The worst thing is that when gamers choose to take it, they will rarely own their acquired products as those usually belong to publishers. 
in addition to the previous background, there have observed three kind of problems that can be easily fixed up: 

- Ownership and management rights. In 2020 everybody is able to buy and sell a house, a car, a computer or, even a t-shirt. Nevertheless, it is inconceivable the lack of ownership of video game related products (key codes, skins, items, etc.) among players, as the final ownership of all those products belong to  the publishers, who usually limit or even forbid the transactions of all those products acquired by the user, sometimes by paying, sometimes by spending long gaming sessions. 

- Digital keys and marketplaces. There are two kinds of marketplaces in video games field: a) A expensive but trustable marketplace ruled by a big publisher and b) An open marketplace where the products tend to be cheaper but of which scammers tend to take advantage of. Thus, when a user go to this last kind of marketplace to buy a license key, an item or anything else related to video games, there is no infalible way to determine if the seller is trustable or not. Actually there is no way to check if a key is legit or if it has been robbed, if it is valid nor if it really represent what was offered by the seller.

- Funding and promotion. Independent developers and small studios have several barriers in order to get funds and promotion for their video game projects. Those barriers move from high funding rates to giving up the ownership of their project, which can be considered clearly abusive. All those barriers consolidate the supremacy of publishers towards independent developers and small studios.

- Although it can be considered that blockchain can help to fix those problems, it must be considered three problem and a consequence related to most blockchain video game projects that also can be fixed: 
Absence of full transparency related to the usage of sale funds together with the concept of that spent.
Exaggerated collection of funds during the token sale phases that results in an unreasonable total startup capitalization that is far away to be related with the startup real value.
Relative low transparency related to the MainNet release where reward rules are rarely explicit and clear for the community. 
As consequence, those three problems are aggravated and exploited in a massive way thanks to the inside info sharing, that paves the way for the exploitation of the market and, finally, centralization.





## MISSION

### NICE1 BLOCKCHAIN:

Taking into account the observed problems in video games and blockchain field, Nice1 blockchain aims to be the nexus between services, studies, blockchain and video game developers and gamers. Nice1 blockchain final mission is to promote video games and blockchain implementation on them from a fair, sustainable and democratic perspective, which means clear rules for business model, public sale, governance system and rewards: 
Video game players and video game community: We want to empower the community giving them the opportunity to choose which video game/blockchain/service projects should be developed and, going further, participate in their development (giving feedback, ideas, etc).

- Video game developers: The aim is to promote the implementation of blockchain in video games in that way that any video game developer could be able to do this without the need of wide blockchain knowledge.

- Blockchain developers: Our mission is to promote the development of blockchain utilities and plugins that facilitates blockchain implementation in video games as well as the setting-up of non-fungible token (NFT) property rights, services, smart contracts and DApps for a decentralized market democratizing the video games ecosystem.

- Studios: Studies will be able to interact with communities and developers, obtaining feedback about their projects, as well as to keep in touch with blockchain and video game developers in order to set up new projects. Likewise, Nice1 blockchain will serve as a great repository of documentation, tools and apps for the implementation of blockchain within video games.

- Services: As part of a video game ecosystem, Nice1 blockchain will promote the development of services such as like marketplaces, auction houses, betting shops, etc. that promote the commercial area of video games field from a sustainable and democratic perspective, considering that the user who acquires something must be the owner of its acquisition.

- Incubator: Nice1 blockchain will serve as incubator through promoting and funding developers and studios with good gaming-related projects that take into account community, despite its bigger or lesser profitability, in order for them to materialize their ideas.


### NICE1 FOUNDATION:

Nice1 Foundation is a figure that aims to set up and manage Nice1 blockchain until it becomes 100% community managed. The Foundation works as nexus between community key players, what mainly means blockchain and video game developers, studios, gamers and, overall, Nice1 users. Developers and, community overall, will be able to send proposals to the Foundation, who will check the legitimacy of those proposals and transfer them to the community, who has the final responsibility to choose if any proposal should be promoted or not. If the community vote is yes, the Foundation will work in order to help for the materialization of this idea with community feedback, economic funds and marketing campaigns. Although the final target is the 100% community management of Nice1 blockchain, once it becomes achieved foundation will remain existent but with a secondary role.





## SOLUTIONS

### Ownership and management rights.

The tokenization of game licenses and other video game related products such as skins or items can be represented by NFT’s, that can provide users real ownership of their acquisitions, which should be stored in their Nice1 blockchain wallets. Moreover, it will be also possible to get authenticated through a Nice1 blockchain wallet. This is a game changer that opens up a range of opportunities for video game developers and publishers in order to be able to create a limited number of video game related products such as license keys, skins or items, making them more valuable for their limited supply condition. Likewise, users who pay for those products will acquire the total and final ownership of those, being able to trade or transfer those with anybody else and in the way they want.


### Digital keys and marketplaces

Marketplaces will become more secure and accurate when using video game related products developed through Nice1 blockchain. The traceability of tokenized licenses and the ability of everybody to verify the legitimacy as well as the usage history of a license key will banish scammers from this field.


#### Financing and promotion

Nice1 Foundation together with Nice1 ecosystem are responsible for the support of projects through funding and marketing without having to claim part of the profits or the copyright or creation of the works from the author. Moreover, developers that prioritize Nice1 blockchain community in their projects will get extra rewarded. In this line, there are three possible scenarios, each one for each kind of development: video game, blockchain and service or smart contract.


#### Video game development scenario

In this scenario, a video game developer transfer a project proposal to Nice1 Foundation in order to join the accelerator/incubator. Nice1 Foundation verifies the legitimacy of the proposal and ask the community if they want this project to join the incubator/accelerator. If the community vote yes, Nice1 Foundation will provide a first payment to the developer for starting the project. The second step starts once the developer release an alpha version of its project. Then, Nice1 Foundation will also ask the community if this project is suited to keep in the incubator/accelerator. In this phase the community will also be able to provide feedback to the developer about its project. If community say yes to keep in the incubator/accelerator, Nice1 Foundation will provide the developer a second payment as well as the community feedback. The third step starts once the developer releases a beta version of its video game. In this step, if the developer takes community into account and offers something exclusive to it like items, skins, characters or even priority access to the game, the developer will receive a third and last payment for the final release. Once the final version of the video game is released, Nice1 Foundation will promote it through marketing campaigns which will lead, lastly, to success for the developer as well as for Nice1 blockchain, as foreign users will know and join Nice1 community.


#### Blockchain implementation

Here, a blockchain developer perform a network implementation in Nice1 blockchain. This implementation might take two ways. It might be accepted by the Nice1 Foundation or it might be transferred to the community to vote if they like or not this implementation. In case the implementation is accepted, the developer will be rewarded by Nice1 Foundation.


#### Service or smart contract implementation

In this case, the developer send a service or smart contract proposal to the foundation. Then, the foundation have two options: accept it or forward this proposal to the community and give it the opportunity to vote. If the proposal is accepted, the developer will be rewarded by Nice1 Foundation as soon as its proposal has been developed and implemented.





## DOCUMENTATION AND UTILITIES

There is no progress without knowledge. As a community project, Nice1 considers shared knowledge as the best knowledge. Thus, it will be promoted the development of a documentation repository that registers every service, process and anything else that means an utility for Nice1 blockchain. This repository will be community-driven and, moreover, will be available for everybody. The benefits of this repository are diverse:
There is a database with development achievements and how to replicate them.
As developers will be able to check what has been done and what has not, they will be able to push for new development achievements and proposals that helps the ecosystem.
Non-community users will be able to check what is nice1 and how it helps the videogame ecosystem.
Newcomers will have something similar to an user manual that facilitates their join into the ecosystem.





## BLOCKCHAIN SOLUTION

### MainNet System & Tokenomics

#### Mainnet System

EOSio has been the blockchain selected to host Nice1 blockchain for three reasons: It is fast, 0% transaction fees and there is a node-based governance system. If it is aimed the implementation and usage of blockchain through video games, this usage must be fast in order to do not disturb the game flow and transactions must charge no fees in order to achieve adoption and scalability. Likewise, the node-based governance system allows the community-management of this blockchain with several differences with regard to EOS and other EOSio blockchains: amount of nodes, rewards system and vote delegation

- Amount of nodes: It is though by several blockchain users that 21 nodes can be considered a synonym of centralization, as this management seems not very distributed. Therefore, Nice1 will be governed not by 21 but 111 nodes (21 producers and 90 in stand-by), which will increase democracy and decentralization.

-Rewards system: Nice1 blockchain considers that stand-by nodes are as much necessary as producers. Therefore, every node, despite if producer or in stand-by, will receive the same reward (1:1). With this reward system, fair competency between established and candidate nodes is promoted.

- Vote delegation: Current situations allows a single node to receive votes and, therefore, benefits. Likewise, this node does not have the obligation to redistribute its benefits among those voters who allowed the node to get such profit. Nice1 blockchain considers it as a gap and will establish a system where voters will be automatically rewarded according to a percentage on the basis of their participation.

In sum, our main net system will lead to a virtuous circle where community and nodes implication is promoted through profits and the ecosystem grows in terms of development and sustainability.


#### Tokenomics

Token Base NICETOKEN - EOSIO TOKEN

-Total Tokens: (Genesis): 50,000,000,000 N1

-Total Tokens Sold: (Public sale): 20,000,000,000
-Percentage Sold: (Public sale): 40.00
-Token Value: (Approx.): $0.00006

Distribution of tokens and locks

Issued tokens are distributed as follows:

-Public sale: 20,000,000,000 (40%)
-Development & Marketing: 25,000,000,000 (50%)
-Partners: 3,000,000,000 (6%)
-Nice1 Foundation: 1,000,000,000 (2%)
-Giveaways: 750,000,000 (1.5%)
-Launch Seed Fund: 250,000,000 (0.5%)

Token lockout period.

Public Sale (40%)
Public sale tokens are unlocked and will be delivered during the first 30 days after the public sale.

Development & Marketing (50%)
Development & Marketing tokens will remain available for the foundation in order to for the execution of community proposals, as well as investment in the development and marketing of the project. They are unblocked in 18 monthly instalments of 1,388,888,888 tokens. Budgets and expenditures will be displayed on a monthly basis.

Partners (6%)
These tokens are available to the foundation for the execution of strategic partners with collaborators. They will be blocked to the partner until the launching of the MainNet, with some exceptions.

Nice1 Foundation (2%)
These tokens are at the disposal of the foundation to cover the expenses of the foundation and its collaborators. The foundation will not have any blocked tokens, the foundation has the duty to expose the expenses made every month.

Giveaways (1.5%)
These tokens will be given away at community support events during the first 2 months after the project's launch. They are not blocked.

Launch Seed Fund (0.5%)
These tokens are distributed among 5 initial collaborators who provided the basic liquidity necessary to carry out the launch of the Nice1 project. These tokens are blocked for 12 months until the launch of the MainNet.



### Fund transparency

The foundation is committed to the publication of the accounting ledger where it will be showed the usage of funds collected from the public sale as well as the usage of tokens managed by the foundation. Moreover, the amount of tokens used or swapped in the market for liquidity purposes will be referenced. This accounting ledger will be updated every month.
